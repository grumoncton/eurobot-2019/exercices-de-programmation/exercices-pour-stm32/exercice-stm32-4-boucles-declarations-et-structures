// vim: spelllang=fr

#include <cstring>
#include <stdio.h>
#include <stdint.h>
#include <mbed.h>
Serial ser(USBTX, USBRX, NULL, 9600);

// En C, il existe plusieurs types de déclarations (statement). Le logique et
// les sélection sont fait avec des déclaration de sélection (selection
// statement). La répétition est fait avec des déclaration d'itération
// (iteration statement).

// D'autre type de déclaration existe, mais elles ne sont pas recommandés.


int main() {

	/**
	 * Partie A: Déclarations de sélection
	 *
	 * if, if / else, if / else if / else, switch
	 *
	 * Comparison operators / relational operators:
	 * https://en.wikipedia.org/wiki/Operators_in_C_and_C%2B%2B#Comparison_operators.2Frelational_operators
	 */

	uint8_t x = 5;
	uint8_t y = 4;

	// Imprimer "x = 5" si et seulement si x est égale à cinq.




	// Imprimer "x ≠ 4" si et seulement si x n'est pas égale à quatre.




	// Imprimer "x ≤ 5" si x est plus petit ou égale à cinq et "x > 5" sinon.






	// Imprimer "y = 4" si y est égale à quatre. Sinon, si y est strictement
	// entre zéro et dix imprimer "0 < y < 10". Imprimer "y ≥ 10" autrement.








	char operation = '+';

	// Imprimer le résultat de l'opération mathématique de la variable `operation`
	// sur les valeurs `x` et `y` pour chacun des opérations suivantes: +, -, *, /



















	/**
	 * Partie B: Boucles
	 *
	 * while, do / while, for
	 */


	// Dans une boucle `while`, incrémenter la variable `x` de `y`. Arrêter la
	// boucle quand `x` n'est pas égale à `5`.





	// Entrer dans une boucle infinie. Sortez de la boucle lorsque `y` est égale à
	// `4`.






	// Imprimer sur le port série une fois avec une boucle do / while.




	// Imprimer sur 10 lignes le numéros de 0 à 9




	// Imprimer sur 4 lignes les numéros dans la variable `numbers`





	// À l'aide d'une boucle `for`, imprimer le message `message` à l'envers.







	/**
	 * Partie C: Structures
	 *
	 * Les structures permettent de regrouper plusieurs variables (ainsi que des
	 * fonctions).
	 */

	// Définir une structure qui permet de stocker la position dans les axes de x
	// et y du robot, ainsi que son angle par rapport à l'axe des x (heading).






	// Créer une instance d'une position avec zéro pour les trois valeurs.






	// Imprimer les trois valeurs de la positon.


	// Définir une structure qui permet de regrouper des commandes et les
	// callbacks associées.






	// Définir les commandes ACK et NACK



	const char receivedCommand[] = "ACK";

	// Exécuter le callback approprier






	return 0;
}
