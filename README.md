<!--
vim: spelllang=fr keymap=accents
-->
# Exercice 4 de programmation pour STM32 : Boucles, déclarations et structures

Les boucles et les déclarations permettent de faire  un code plus complexe avec
de la logique et la généralisation des instruction répétés.

## Marches à suivre

### Téléchargement
Exécutez les commandes suivantes dans *Git Bash*.
1. Créez un dossier pour le répertoire:
	```
	mkdir -p ~/code/grum/eurobot2019/exercices/exercices-pour-stm32
	```
1. Clonez le répertoire:
	```
	git clone ssh://gitlab@gitlab.robitaille.host:49/GRUM/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-4-boucles-declarations-et-structures.git ~/code/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-4-boucles-declarations-et-structures
	```
1. Ouvrez le dossier:
	```
	cd ~/code/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-4-boucles-declarations-et-structures
	```
1. Ouvrez le dossier dans *Visual Sutdio Code*:
	```
	code .
	```

### Programmation
Ouvrez le fichier `src/main.cpp` et suivez les directives.

### Compilation
Une fois que le code est écrit, compiler le programme avec la commande
`PlatformIO: Build` (tapez <kbd>ctrl-shift-p</kbd> pour ouvrir la ligne de
commande) ou avec le raccourci <kbd>ctrl-alt-b</kbd>.

### Téléversement
Si vous avez accès à un microcontrôleur (on en a commandé!), téléversez le
programme compilé avec la commande `PlatformIO: Upload` (tapez
<kbd>ctrl-shift-p</kbd> pour ouvrir la ligne de commande) ou avec le raccourci
<kbd>ctrl-alt-u</kbd>.
